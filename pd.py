##
## This file is part of the libsigrokdecode project.
##
## Copyright (C) 2015 Bart de Waal <bart@waalamo.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, see <http://www.gnu.org/licenses/>.
##
import sigrokdecode as srd
from math import ceil
rxtx_channels = ('RX', 'TX')


class capturePCAP():
    def __init__(self):
        self.pi = 0
        pass

    def putb(self, ts, data):
        self.put(ts, ts, self.out_binary, data)

    def pcapWriteHeader(self):
        header = b'\xa1\xb2\xc3\xd4'#magic
        header += b'\x00\x02'#major version
        header += b'\x00\x04'#minor version
        header += b'\x00\x00\x00\x00'#tz
        header += b'\x00\x00\x00\x00'#sigfigs
        header += b'\x00\x00\xff\xff'#snaplen
        header += b'\x00\x00\x01\x01'#data link type
        self.put(0, 0, self.out_binary, [0, header])

    def pcapWriteFrame(self,frame):
        print(frame)
        startFrame = frame[0][1] / self.sampleRate

        startFrameS = int(startFrame).to_bytes(4,'big')
        startFrameUS = int((startFrame % 1) * 1000000).to_bytes(4,'big')
        body = startFrameS
        body += startFrameUS
        #body = self.pi.to_bytes(4,'big')
        #body += self.pi.to_bytes(4,'big')
        self.pi += 1
        body += (len(frame)).to_bytes(4,'big')
        body += (len(frame)).to_bytes(4,'big')
        for data, start, end in frame:
            body += data.to_bytes(1,'big')
        print(body)
        self.put(0, 0, self.out_binary, [0, body])
        

class Decoder(srd.Decoder,capturePCAP):
    api_version = 3
    id = 'fdl'
    name = 'Fieldbus Data Link'
    longname = 'Fieldbus Data Link'
    desc = 'Fieldbus protocol for industrial applications, utilized in Profibus.'
    license = 'gplv3+'
    inputs = ['uart']
    outputs = ['profibus']
    tags = ['industrial']
    options = (
        {'id': 'baudrate', 'desc': 'Baud rate', 'default': 115200},
    )
    annotations = (
        ('sd', 'Start Delimiter'),
        ('le', 'Length'),
        ('ler', 'Length repeated'),
        ('fc', 'Function Code'),
        ('da', 'Destination Address'),
        ('sa', 'Source Address'),
        ('dsap', 'Destination Service Access Point'),
        ('ssap', 'Source Service Access Point'),
        ('pdu', 'Protocol Data Unit'),
        ('fcs', 'Frame Checking Sequence'),
        ('ed', 'End Delimiter'),

        ('sd1', 'No data'),
        ('sd2', 'Variable length data'),
        ('sd3', 'Fixed length data'),
        ('sd4', 'Token'),
        ('sd5', 'Short ACK'),

        ('syn', 'SYN'),

        ('error', 'error'),
    )
    annotation_rows = (
        ('bytes', 'Bytes', (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
        ('frame', 'Frame', (11, 12, 13, 14, 15, 16)),
        ('error-indicators', 'Errors in frame', (17,)),
    )
    binary = (
        ('pcap', 'PCAP format'),
    )

    def __init__(self):
        self.reset()

    def reset(self):
        self.frame = []
        self.frameStart = -1
        self.lastDataEnd = -1
        self.baudRate = 187000
        self.sampleRate = 1
        self.synPeriod = 0
        capturePCAP.__init__(self)


    def start(self):
        self.out_ann = self.register(srd.OUTPUT_ANN)
        self.out_binary = self.register(srd.OUTPUT_BINARY)
        self.pcapWriteHeader()

    def metadata(self, key, value):
        if key == srd.SRD_CONF_SAMPLERATE:
            self.sampleRate = value
            self.synPeriod = int((33 * value) / self.options['baudrate'])  #syn length in samples

    def puta(self, start, end, ann_str, message):
        '''Put an annotation from start to end, with ann as a
        string. This means you don't have to know the ann's
        number to write annotations to it.'''
        ann = [s[0] for s in self.annotations].index(ann_str)
        self.put(start, end, self.out_ann, [ann, [message]])

    def decode(self, ss, es, data):
        ptype, rxtx, pdata = data

        # Ignore unknown/unsupported ptypes.
        #if ptype not in ('STARTBIT', 'DATA', 'STOPBIT'):
        #    return
        if ptype != 'FRAME':
            return

        #print(f'timings: {self.lastDataEnd} {ss}')
        #if self.lastDataEnd == ss:
        if abs(self.lastDataEnd - ss) <= 20:
            self.frame.append([pdata[0],ss,es])
        elif self.lastDataEnd == -1:
            self.frame = [[pdata[0],ss,es]]
            self.frameStart = ss
        else:
            self.decodeFrame()
            self.pcapWriteFrame(self.frame)
            self.frame = [[pdata[0],ss,es]]
            self.frameStart = ss
            if self.lastDataEnd != -1:
                #self.put(int(self.frameStart - 3300), self.frameStart, self.out_ann,[16,['Sync porch','SYN']])
                self.put(self.frameStart - self.synPeriod, self.frameStart, self.out_ann,[16,['Sync porch','SYN']])
                #print(f'syn {(self.frameStart - (33 / self.baudRate) * 1000000)} to {self.frameStart}')
                #print(str(self.options))
            

        self.lastDataEnd = es

    def decodeFrame(self):
        if self.frame[0][0] == (b'\x10')[0]:
            self.decodeSD1()
        elif self.frame[0][0] == (b'\x68')[0]:
            self.decodeSD2()
        elif self.frame[0][0] == (b'\xA2')[0]:
            self.decodeSD3()
        elif self.frame[0][0] == (b'\xDC')[0]:
            self.decodeSD4()
        elif self.frame[0][0] == (b'\xE5')[0]:
            self.decodeSD5()
        else:
            print('frame error')

    def decodeSD1(self):
        self.put(self.frameStart,self.lastDataEnd, self.out_ann,[11,['No data']])
        if len(self.frame) != 6:
            self.errorPacketLength(6,len(self.frame))
        self.putSyn()
        self.putStartDelimiter(0)
        self.putDestinationAddress(1)
        self.putSourceAddress(2)
        self.putFunctionCode(3)
        self.checkFCS(self.frame[1:-2],self.frame[-2][0])
        self.putFrameCheckingSequence(4)
        self.putEndDelimiter(5)
        
    def decodeSD2(self):
        self.put(self.frameStart,self.lastDataEnd, self.out_ann,[12,['Variable length data']])
        self.putSyn()#Only needed when doing a request. Not needed for replies.
        self.putStartDelimiter(0)
        self.putLength(1)
        if len(self.frame[3:-3]) != self.getLength(1):
            self.errorPacketLength(self.getLength(1),len(self.frame[3:-3]))
        self.putLengthRepeated(2)
        self.putStartDelimiter(3)
        self.putDestinationAddress(4)
        self.putSourceAddress(5)
        self.putFunctionCode(6)
        self.putDestinationServiceAccessPoint(7)
        self.putSourceSourceServiceAccessPoint(8)

        pdu = ''
        for b in self.frame[9:-3]:
            pdu += f'{b[0]:x}'.zfill(2)
        self.putProtocolDataUnit(9,-3,pdu)
        self.checkFCS(self.frame[4:-2],self.frame[-2][0])
        #self.checkFCS(self.frame[1:-2],self.frame[-2][0])
        self.putFrameCheckingSequence(-2)
        self.putEndDelimiter(-1)

    def decodeSD3(self):#untested, incomplete
        self.put(self.frameStart,self.lastDataEnd, self.out_ann,[13,['Fixed length data']])
        self.putSyn()
        
    def decodeSD4(self):
        self.put(self.frameStart,self.lastDataEnd, self.out_ann,[14,['Token']])
        self.putSyn()
        if len(self.frame) != 3:
            self.errorPacketLength(3,len(self.frame))
        self.putStartDelimiter(0)
        self.putDestinationAddress(1)
        self.putSourceAddress(2)

    def decodeSD5(self):#untested
        self.put(self.frameStart,self.lastDataEnd, self.out_ann,[15,['Short ACK']])
        if len(self.frame) != 1:
            self.errorPacketLength(1,len(self.frame))
        self.putStartDelimiter(0)


    def putSyn(self):
        if self.lastDataEnd != -1:
            self.put(self.frameStart - self.synPeriod, self.frameStart, self.out_ann,[16,['Sync porch','SYN']])

    def putStartDelimiter(self,position):
        self.put(self.frame[position][1],self.frame[position][2], self.out_ann,[0,[f'Start Delimiter: {self.frame[position][0]:#x}',f'SD {self.frame[position][0]:#x}','SD']])

    def putLength(self,position):
        self.put(self.frame[position][1],self.frame[position][2], self.out_ann,[1,[f'Paket Length: {self.frame[position][0]:#x}',f'LE {self.frame[position][0]:#x}','LE']])

    def putLengthRepeated(self,position):
        self.put(self.frame[position][1],self.frame[position][2], self.out_ann,[2,[f'Paket Length repeated: {self.frame[position][0]:#x}',f'LEr {self.frame[position][0]:#x}','LEr']])

    def putFunctionCode(self,position):
        self.put(self.frame[position][1],self.frame[position][2], self.out_ann,[3,[f'Function Code: {self.frame[position][0]:#x}',f'FC {self.frame[position][0]:#x}','FC']])

    def putDestinationAddress(self,position):
        self.put(self.frame[position][1],self.frame[position][2], self.out_ann,[4,[f'Destination Address: {self.frame[position][0]:#x}',f'DA {self.frame[position][0]:#x}','DA']])

    def putSourceAddress(self,position):
        self.put(self.frame[position][1],self.frame[position][2], self.out_ann,[5,[f'Source Address: {self.frame[position][0]:#x}',f'SA {self.frame[position][0]:#x}','SA']])

    def putDestinationServiceAccessPoint(self,position):
        self.put(self.frame[position][1],self.frame[position][2], self.out_ann,[6,[f'Destination Service Access Point: {self.frame[position][0]:#x}',f'DSAP {self.frame[position][0]:#x}','DSAP']])

    def putSourceSourceServiceAccessPoint(self,position):
        self.put(self.frame[position][1],self.frame[position][2], self.out_ann,[7,[f'Source Service Access Point: {self.frame[position][0]:#x}',f'SSAP {self.frame[position][0]:#x}','SSAP']])

    def putProtocolDataUnit(self,fromPos,toPos,pdu):
        self.put(self.frame[fromPos][1],self.frame[toPos][2], self.out_ann,[8,[f'Protocol Data Unit: {pdu}','Protocol Data Unit','PDU']])

    def putFrameCheckingSequence(self,position):
        self.put(self.frame[position][1],self.frame[position][2], self.out_ann,[9,[f'Frame Checking Sequence: {self.frame[position][0]:#x}',f'FCS {self.frame[position][0]:#x}','FCS']])

    def putEndDelimiter(self,position):
        self.put(self.frame[position][1],self.frame[position][2], self.out_ann,[10,[f'End Delimiter: {self.frame[position][0]:#x}',f'ED {self.frame[position][0]:#x}','ED']])



    def getLength(self,position):
        return self.frame[position][0]

    def checkFCS(self,data,fcs):
        checksum = 0
        for b in data:
            checksum += b[0]
        if (checksum % 256) != fcs:
            self.put(self.frameStart,self.lastDataEnd, self.out_ann,[17,[f'Error: Frame Checking Sequence is wrong {checksum % 255}','FCS']])
            




    def errorPacketLength(self,normalLength,packetlength):
        self.put(self.frameStart,self.lastDataEnd, self.out_ann,[17,[f'Error: Packet length is {packetlength} but should be {normalLength}','Length']])

